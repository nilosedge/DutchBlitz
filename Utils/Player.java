package Utils;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;

public class Player {
	
	private Vector<Card> blitzpile;
	private Vector<Card> discardpile1;
	private Vector<Card> discardpile2;
	private Vector<Card> discardpile3;
	private Vector<Card> flippile;
	private Vector<Card> flippedpile;
	
	private ArrayList<Card> cards = new ArrayList<Card>();
	private int flipcount = 0;
	private int lastflipcount = 0;
	
	//private int points = 0;

	public Player() {
		blitzpile = new Vector<Card>(10);
		discardpile1 = new Vector<Card>(10);
		discardpile2 = new Vector<Card>(10);
		discardpile3 = new Vector<Card>(10);
		flippedpile = new Vector<Card>(30);
		flippile = new Vector<Card>(30);
		Color c[] = new Color[4];
		c[Card.RED] = Color.RED;
		c[Card.YEL] = Color.YELLOW;
		c[Card.GRE] = Color.GREEN;
		c[Card.BLU] = Color.BLUE;
		//name = string;
		for(int i = 0; i < 40; i++) {
			cards.add(new Card(c[(int)Math.floor(i / 10)], (i%10) + 1, ((int)Math.floor(i / 10)) % 2, i));
		}
		newGame();
	}
	
	public void newGame() {
		clearPoints();
		emptyQueues();
		shuffle();
		deal();
	}
	
	private void clearPoints() {
		//points = 0;
	}

	public void newRound() {
		calculatePoints();
		emptyQueues();
		shuffle();
		deal();
	}
	
	private void calculatePoints() {
		
		
	}

	private void emptyQueues() {
		blitzpile.clear();
		discardpile1.clear();
		discardpile2.clear();
		discardpile3.clear();
		flippedpile.clear();
		flippile.clear();
	}
	
	public void flip() {

		if(flippile.isEmpty()) {
			if(lastflipcount == flippedpile.size()) { flipcount ++; }
			if(lastflipcount == flippedpile.size() && flipcount == 3 && flippedpile.size() > 0) {
				flippedpile.add(flippedpile.remove(0));
				flipcount = 0;
			}
			lastflipcount  = flippedpile.size();
			flippile = flippedpile;
			flippedpile = new Vector<Card>(30);
			
		}
		if(!flippile.isEmpty()) flippedpile.add(flippile.remove(0));
		if(!flippile.isEmpty()) flippedpile.add(flippile.remove(0));
		if(!flippile.isEmpty()) flippedpile.add(flippile.remove(0));
	}

	public void shuffle() {
		Collections.shuffle(cards);
	}
	
	public void deal() {
		int i = 0;
		while(i < 10) {
			blitzpile.add(cards.get(i++));
		}
		while(i < 37) {
			flippile.add(cards.get(i++));
		}
		discardpile1.add(cards.get(i++));
		discardpile2.add(cards.get(i++));
		discardpile3.add(cards.get(i++));
	}


	public Card getFlippedPileTopCard() {
		if(flippedpile.isEmpty()) return null;
		else return flippedpile.lastElement();
	}
	public void removeTopFlippedCard() {
		if(!flippedpile.isEmpty()) {
			flippedpile.removeElementAt(flippedpile.size() - 1);
		}
	}
	public void removeTopDisCardPile1() {
		if(!discardpile1.isEmpty()) {
			discardpile1.removeElementAt(discardpile1.size() - 1);
		}
	}
	public void removeTopDisCardPile2() {
		if(!discardpile2.isEmpty()) {
			discardpile2.removeElementAt(discardpile2.size() - 1);
		}
	}
	public void removeTopDisCardPile3() {
		if(!discardpile3.isEmpty()) {
			discardpile3.removeElementAt(discardpile3.size() - 1);
		}
	}
	public Card getDisCardPile1TopCard() {
		if(discardpile1.isEmpty()) return null;
		else return discardpile1.lastElement();
	}
	public Card getDisCardPile2TopCard() {
		if(discardpile2.isEmpty()) return null;
		else return discardpile2.lastElement();
	}
	public Card getDisCardPile3TopCard() {
		if(discardpile3.isEmpty()) return null;
		else return discardpile3.lastElement();
	}

	public Card getBlitzCardPileTopCard() {
		if(blitzpile.isEmpty()) return null;
		else return blitzpile.lastElement();
	}
	public void removeTopBlitzCardPile() {
		if(!blitzpile.isEmpty()) {
			blitzpile.removeElementAt(blitzpile.size() - 1);
		}
	}

	public void addDisCardPile1TopCard(Card card) {
		discardpile1.add(card);
	}
	public void addDisCardPile2TopCard(Card card) {
		discardpile2.add(card);
	}
	public void addDisCardPile3TopCard(Card card) {
		discardpile3.add(card);
	}
}
