package Utils;

import java.awt.Color;

public class Card {
	
	public static int RED = 0;
	public static int YEL = 1;
	public static int GRE = 2;
	public static int BLU = 3;
	public static int MALE = 0;
	public static int FEMALE = 1;
	
	public Color color;
	public int number;
	public int gender;
	public int cardnumber;
	
	public Card(Color c, int n, int g, int cn) {
		color = c;
		number = n;
		gender = g;
		cardnumber = cn;
		//System.out.println("Color: " + c + " Number: " + n + " Gender: " + g + " Cardnumber: " + cn);
	}
	
	public String toString() {
		return "Color: " + color + " Number: " + number + " Gender: " + gender + " Cardnumber: " + cardnumber;
	}
}
