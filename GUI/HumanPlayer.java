package GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

import Utils.Card;
import Utils.Player;

public class HumanPlayer extends Thread {
	private static final long serialVersionUID = 2057605404011119204L;
	
	private Player player = new Player();
	private JTextComponent jtf = new JTextField("Test");
	private JTextComponent jtd1 = new JTextField("Test");
	private JTextComponent jtd2 = new JTextField("Test");
	private JTextComponent jtd3 = new JTextField("Test");
	private JTextComponent jtb = new JTextField("Test");
	
	private PlayArea playarea;
	
	public HumanPlayer(PlayArea pa, JPanel jp) {
		playarea = pa;
		jp.setPreferredSize(new Dimension(400, 100));
		jp.setBorder(BorderFactory.createLineBorder(new Color(0)));
		jp.add(makeBlitzPile());
		jp.add(makeDisCardPile1());
		jp.add(makeDisCardPile2());
		jp.add(makeDisCardPile3());
		jp.add(makeFlipPile());
		jp.add(makeFlipButton());
	}
	
	private JPanel makeBlitzPile() {
		JPanel jp = new JPanel();
		jp.setBorder(BorderFactory.createLineBorder(new Color(0)));
		jp.setPreferredSize(new Dimension(35, 50));
		Card card = player.getBlitzCardPileTopCard();
		if(card != null) jtb.setText(Integer.toString(card.number));
		else jtb.setText("NN");
		jp.addMouseListener(
				new MouseListener() {
					public void mouseClicked(MouseEvent arg0) {
						//player.flip();
						Card card = player.getBlitzCardPileTopCard();
						if(card == null) {
							playarea.Blitz();
							jtb.setText("BB");
						} else {
							card = playarea.play(card);
							if(card == null) {
								player.removeTopBlitzCardPile();
								card = player.getBlitzCardPileTopCard();
								if(card != null) jtb.setText(Integer.toString(card.number));
								else jtb.setText("BB");
							} else if(player.getDisCardPile1TopCard() == null) {
								player.addDisCardPile1TopCard(card);
								player.removeTopBlitzCardPile();
								jtd1.setText(Integer.toString(card.number));
								card = player.getBlitzCardPileTopCard();
								if(card != null) jtb.setText(Integer.toString(card.number));
								else jtb.setText("NN");
							} else if(player.getDisCardPile2TopCard() == null) {
								player.addDisCardPile2TopCard(card);
								player.removeTopBlitzCardPile();
								jtd2.setText(Integer.toString(card.number));
								card = player.getBlitzCardPileTopCard();
								if(card != null) jtb.setText(Integer.toString(card.number));
								else jtb.setText("NN");
							} else if(player.getDisCardPile3TopCard() == null) {
								player.addDisCardPile3TopCard(card);
								player.removeTopBlitzCardPile();
								jtd3.setText(Integer.toString(card.number));
								card = player.getBlitzCardPileTopCard();
								if(card != null) jtb.setText(Integer.toString(card.number));
								else jtb.setText("NN");
							}
						}
					}
					public void mouseReleased(MouseEvent arg0) {}
					public void mouseEntered(MouseEvent e) {}
					public void mouseExited(MouseEvent e) {}
					public void mousePressed(MouseEvent e) {}
				}
			);
		jp.add(jtb);
		return jp;
	}
	private JPanel makeDisCardPile1() {
		JPanel jp = new JPanel();
		jp.setBorder(BorderFactory.createLineBorder(new Color(0)));
		jp.setPreferredSize(new Dimension(35, 50));
		Card card = player.getDisCardPile1TopCard();
		if(card != null) jtd1.setText(Integer.toString(card.number));
		else jtd1.setText("NN");
		jp.addMouseListener(
				new MouseListener() {
					public void mouseClicked(MouseEvent arg0) {
						//player.flip();
						Card card = player.getDisCardPile1TopCard();
						if(card == null) {
							jtd1.setText("NN");
						} else {
							card = playarea.play(card);
							if(card == null) {
								player.removeTopDisCardPile1();
								card = player.getDisCardPile1TopCard();
								if(card != null) jtd1.setText(Integer.toString(card.number));
								else jtd1.setText("NN");
							}
						}
					}
					public void mouseReleased(MouseEvent arg0) {}
					public void mouseEntered(MouseEvent e) {}
					public void mouseExited(MouseEvent e) {}
					public void mousePressed(MouseEvent e) {}
				}
			);
		jp.add(jtd1);
		return jp;
	}
	private JPanel makeDisCardPile2() {
		JPanel jp = new JPanel();
		jp.setBorder(BorderFactory.createLineBorder(new Color(0)));
		jp.setPreferredSize(new Dimension(35, 50));
		Card card = player.getDisCardPile2TopCard();
		if(card != null) jtd2.setText(Integer.toString(card.number));
		else jtd2.setText("NN");
		jp.addMouseListener(
				new MouseListener() {
					public void mouseClicked(MouseEvent arg0) {
						//player.flip();
						Card card = player.getDisCardPile2TopCard();
						if(card == null) {
							jtd2.setText("NN");
						} else {
							card = playarea.play(card);
							if(card == null) {
								player.removeTopDisCardPile2();
								card = player.getDisCardPile2TopCard();
								if(card != null) jtd2.setText(Integer.toString(card.number));
								else jtd2.setText("NN");
							}
						}
					}
					public void mouseReleased(MouseEvent arg0) {}
					public void mouseEntered(MouseEvent e) {}
					public void mouseExited(MouseEvent e) {}
					public void mousePressed(MouseEvent e) {}
				}
			);
		jp.add(jtd2);
		return jp;
	}
	private JPanel makeDisCardPile3() {
		JPanel jp = new JPanel();
		jp.setBorder(BorderFactory.createLineBorder(new Color(0)));
		jp.setPreferredSize(new Dimension(35, 50));
		Card card = player.getDisCardPile3TopCard();
		if(card != null) jtd3.setText(Integer.toString(card.number));
		else jtd3.setText("NN");
		jp.addMouseListener(
				new MouseListener() {
					public void mouseClicked(MouseEvent arg0) {
						//player.flip();
						Card card = player.getDisCardPile3TopCard();
						if(card == null) {
							jtd3.setText("NN");
						} else {
							card = playarea.play(card);
							if(card == null) {
								player.removeTopDisCardPile3();
								card = player.getDisCardPile3TopCard();
								if(card != null) jtd3.setText(Integer.toString(card.number));
								else jtd3.setText("NN");
							}
						}
					}
					public void mouseReleased(MouseEvent arg0) {}
					public void mouseEntered(MouseEvent e) {}
					public void mouseExited(MouseEvent e) {}
					public void mousePressed(MouseEvent e) {}
				}
			);
		jp.add(jtd3);
		return jp;
	}
	private JPanel makeFlipPile() {
		JPanel jp = new JPanel();
		jp.setBorder(BorderFactory.createLineBorder(new Color(0)));
		jp.setPreferredSize(new Dimension(35, 50));
		Card card = player.getFlippedPileTopCard();
		if(card != null) jtf.setText(Integer.toString(card.number));
		else jtf.setText("NN");
		jp.addMouseListener(
				new MouseListener() {
					public void mouseClicked(MouseEvent arg0) {
						//player.flip();
						Card card = player.getFlippedPileTopCard();
						if(card == null) {
							jtf.setText("NN");
						} else {
							card = playarea.play(card);
							if(card == null) {
								player.removeTopFlippedCard();
								card = player.getFlippedPileTopCard();
								if(card != null) jtf.setText(Integer.toString(card.number));
								else jtf.setText("NN");
							}
						}
					}
					public void mouseReleased(MouseEvent arg0) {}
					public void mouseEntered(MouseEvent e) {}
					public void mouseExited(MouseEvent e) {}
					public void mousePressed(MouseEvent e) {}
				}
			);
		jp.add(jtf);
		return jp;
	}
	private JButton makeFlipButton() {
		JButton jb = new JButton("Flip");
		jb.addActionListener(
			new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					player.flip();
					Card card = player.getFlippedPileTopCard();
					if(card != null) jtf.setText(Integer.toString(card.number));
					else jtf.setText("NN");
				}
			}
		);
		return jb;
	}
	
	public void run() {
		while(true) {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
}
