package GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Utils.Card;

public class PlayArea extends JPanel {
	private static final long serialVersionUID = 305638934929859310L;
	private int panelcount = 16;
	private JPanel[] panels = new JPanel[panelcount];
	private JTextField[] texts = new JTextField[panelcount]; 
	private boolean blitzed = false;
	private Vector<Card>[] piles = new Vector[panelcount];

	public PlayArea() {
		setBorder(BorderFactory.createLineBorder(new Color(0)));
		setPreferredSize(new Dimension(400, 400));
		for(int i = 0; i < panelcount; i++) {
			JPanel jp = new JPanel();
			texts[i] = new JTextField("Test");
			texts[i].setText("NN");
			jp.setBackground(Color.BLACK);
			jp.setBorder(BorderFactory.createLineBorder(new Color(0)));
			jp.setPreferredSize(new Dimension(80, 90));
			jp.add(texts[i]);
			add(jp);
			panels[i] = jp;
			piles[i] = new Vector<Card>(10);
		}
		
		
	}

	public Card play(Card card) {
		//if(blitzed == true) { return card; }
		
		for(int i = 0; i < panelcount; i++) {
			if(piles[i].isEmpty() && card.number == 1) {
				piles[i].add(card);
				panels[i].setBackground(card.color);
				texts[i].setText(Integer.toString(piles[i].lastElement().number));
				System.out.println("Card played: " + card);
				return null;
			}
			if(
					!piles[i].isEmpty() &&
					(card.number - 1) == piles[i].lastElement().number &&
					card.color == piles[i].lastElement().color
			) {
				piles[i].add(card);
				//panels[i].setBackground(card.color);
				texts[i].setText(Integer.toString(piles[i].lastElement().number));
				System.out.println("Card played: " + card);
				return null;
			}
		}
		return card;
	}

	public void Blitz() {
		//System.out.println("We have a winner!!!!!");
		blitzed = true;
	}
	
	
}
