package GUI;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

import Utils.Card;
import Utils.Player;

public class ComputerPlayer extends Thread {
	private static final long serialVersionUID = 2057605404011119204L;
	
	private Player player = new Player();
	private JTextComponent jtf = new JTextField("Test");
	private JTextComponent jtd1 = new JTextField("Test");
	private JTextComponent jtd2 = new JTextField("Test");
	private JTextComponent jtd3 = new JTextField("Test");
	private JTextComponent jtb = new JTextField("Test");
	
	private JPanel bp;
	private JPanel dp1;
	private JPanel dp2;
	private JPanel dp3;
	private JPanel fp;
	
	private int pw = 50;
	private int ph = 80;
	
	private PlayArea playarea;
	public ComputerPlayer(PlayArea pa, JPanel jp) {
		playarea = pa;
		jp.setPreferredSize(new Dimension(400, 100));
		jp.setBorder(BorderFactory.createLineBorder(new Color(0)));
		jp.add(makeBlitzPile());
		jp.add(makeDisCardPile1());
		jp.add(makeDisCardPile2());
		jp.add(makeDisCardPile3());
		jp.add(makeFlipPile());
		//jp.add(makeFlipButton());
	}
	private JPanel makeBlitzPile() {
		bp = new JPanel();
		bp.setBorder(BorderFactory.createLineBorder(new Color(0)));
		bp.setPreferredSize(new Dimension(pw, ph));
		Card card = player.getBlitzCardPileTopCard();
		if(card != null) {
			jtb.setText(Integer.toString(card.number));
			bp.setBackground(card.color);
		}
		else {
			jtb.setText("NN");
			bp.setBackground(Color.BLACK);
		}
		bp.add(jtb);
		return bp;
	}
	private JPanel makeDisCardPile1() {
		dp1 = new JPanel();
		dp1.setBorder(BorderFactory.createLineBorder(new Color(0)));
		dp1.setPreferredSize(new Dimension(pw, ph));
		Card card = player.getDisCardPile1TopCard();
		if(card != null) {
			jtd1.setText(Integer.toString(card.number));
			dp1.setBackground(card.color);
		}
		else {
			jtd1.setText("NN");
			dp1.setBackground(Color.BLACK);
		}
		dp1.add(jtd1);
		return dp1;
	}
	private JPanel makeDisCardPile2() {
		dp2 = new JPanel();
		dp2.setBorder(BorderFactory.createLineBorder(new Color(0)));
		dp2.setPreferredSize(new Dimension(pw, ph));
		Card card = player.getDisCardPile2TopCard();
		if(card != null) {
			jtd2.setText(Integer.toString(card.number));
			dp2.setBackground(card.color);
		}
		else {
			jtd2.setText("NN");
			dp2.setBackground(Color.BLACK);
		}
		dp2.add(jtd2);
		return dp2;
	}
	private JPanel makeDisCardPile3() {
		dp3 = new JPanel();
		dp3.setBorder(BorderFactory.createLineBorder(new Color(0)));
		dp3.setPreferredSize(new Dimension(pw, ph));
		Card card = player.getDisCardPile3TopCard();
		if(card != null) {
			jtd3.setText(Integer.toString(card.number));
			dp3.setBackground(card.color);
		}
		else {
			jtd3.setText("NN");
			dp3.setBackground(Color.BLACK);
		}
		dp3.add(jtd3);
		return dp3;
	}
	private JPanel makeFlipPile() {
		fp = new JPanel();
		fp.setBorder(BorderFactory.createLineBorder(new Color(0)));
		fp.setPreferredSize(new Dimension(pw, ph));
		Card card = player.getFlippedPileTopCard();
		if(card != null) {
			jtf.setText(Integer.toString(card.number));
			fp.setBackground(card.color);
		}
		else {
			jtf.setText("NN");
			fp.setBackground(Color.BLACK);
		}
		fp.add(jtf);
		return fp;
	}
	
	private boolean playBlitzPile() {
		Card card = player.getBlitzCardPileTopCard();
		if(card == null) {
			playarea.Blitz();
			jtb.setText("BB");
			bp.setBackground(Color.BLACK);
			return false;
		} else {
			card = playarea.play(card);
			if(card == null) {
				//playarea.Blitz();
				player.removeTopBlitzCardPile();
				card = player.getBlitzCardPileTopCard();
				if(card != null) {
					jtb.setText(Integer.toString(card.number));
					bp.setBackground(card.color);
				}
				else {
					jtb.setText("BB");
					bp.setBackground(Color.BLACK);
				}
				return true;
			} else if(player.getDisCardPile1TopCard() == null) {
				player.addDisCardPile1TopCard(card);
				player.removeTopBlitzCardPile();
				jtd1.setText(Integer.toString(card.number));
				dp1.setBackground(card.color);
				card = player.getBlitzCardPileTopCard();
				if(card != null) {
					jtb.setText(Integer.toString(card.number));
					bp.setBackground(card.color);
				}
				else {
					jtb.setText("NN");
					bp.setBackground(Color.BLACK);
				}
				return true;
			} else if(player.getDisCardPile2TopCard() == null) {
				player.addDisCardPile2TopCard(card);
				player.removeTopBlitzCardPile();
				jtd2.setText(Integer.toString(card.number));
				dp2.setBackground(card.color);
				card = player.getBlitzCardPileTopCard();
				if(card != null) {
					jtb.setText(Integer.toString(card.number));
					bp.setBackground(card.color);
				}
				else {
					jtb.setText("NN");
					bp.setBackground(Color.BLACK);
				}
				return true;
			} else if(player.getDisCardPile3TopCard() == null) {
				player.addDisCardPile3TopCard(card);
				player.removeTopBlitzCardPile();
				jtd3.setText(Integer.toString(card.number));
				dp3.setBackground(card.color);
				card = player.getBlitzCardPileTopCard();
				if(card != null) {
					jtb.setText(Integer.toString(card.number));
					bp.setBackground(card.color);
				}
				else {
					jtb.setText("NN");
					bp.setBackground(Color.BLACK);
				}
				return true;
			}
		}
		return false;
	}
	private boolean playDisCardPile1() {
		Card card = player.getDisCardPile1TopCard();
		if(card == null) {
			jtd1.setText("NN");
			dp1.setBackground(Color.BLACK);
			return false;
		} else {
			card = playarea.play(card);
			if(card == null) {
				player.removeTopDisCardPile1();
				card = player.getDisCardPile1TopCard();
				if(card != null) {
					jtd1.setText(Integer.toString(card.number));
					dp1.setBackground(card.color);
				}
				else {
					jtd1.setText("NN");
					dp1.setBackground(Color.BLACK);
				}
				return true;
			}
		}
		return false;
	}
	private boolean playDisCardPile2() {
		Card card = player.getDisCardPile2TopCard();
		if(card == null) {
			jtd2.setText("NN");
			dp2.setBackground(Color.BLACK);
			return false;
		} else {
			card = playarea.play(card);
			if(card == null) {
				player.removeTopDisCardPile2();
				card = player.getDisCardPile2TopCard();
				if(card != null) {
					jtd2.setText(Integer.toString(card.number));
					dp2.setBackground(card.color);
				}
				else {
					jtd2.setText("NN");
					dp2.setBackground(Color.BLACK);
				}
				return true;
			}
		}
		return false;
	}
	private boolean playDisCardPile3() {
		Card card = player.getDisCardPile3TopCard();
		if(card == null) {
			jtd3.setText("NN");
			dp3.setBackground(Color.BLACK);
			return false;
		} else {
			card = playarea.play(card);
			if(card == null) {
				player.removeTopDisCardPile3();
				card = player.getDisCardPile3TopCard();
				if(card != null) {
					jtd3.setText(Integer.toString(card.number));
					dp3.setBackground(card.color);
				}
				else {
					jtd3.setText("NN");
					dp3.setBackground(Color.BLACK);
				}
				return true;
			}
		}
		return false;
	}
	private boolean playFlipPile() {
		Card card = player.getFlippedPileTopCard();
		if(card == null) {
			jtf.setText("NN");
			fp.setBackground(Color.BLACK);
			return false;
		} else {
			card = playarea.play(card);
			if(card == null) {
				player.removeTopFlippedCard();
				card = player.getFlippedPileTopCard();
				if(card != null) {
					jtf.setText(Integer.toString(card.number));
					fp.setBackground(card.color);
				}
				else {
					jtf.setText("NN");
					fp.setBackground(Color.BLACK);
				}
				return true;
			}
		}
		return false;
	}

	private void flipPile() {
		player.flip();
		Card card = player.getFlippedPileTopCard();
		if(card != null) {
			jtf.setText(Integer.toString(card.number));
			fp.setBackground(card.color);
		}
		else {
			jtf.setText("NN");
			fp.setBackground(Color.BLACK);
		}
	}
	
	public void run() {
		while(true) {
			try {
				//System.out.println("Seeing if this player sleeps");
				if(playBlitzPile() == false) {
					if(playDisCardPile1() == true) {
						
					} else if(playDisCardPile2() == true) {
						
					} else if(playDisCardPile3() == true) {
						
					} else if(playFlipPile() == true) {
						
					} else {
						flipPile();
					}
				}
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
