package GUI;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class MainFrame extends JFrame {
	private static final long serialVersionUID = -3808746525900707881L;

	public MainFrame() {
		super("Dutch Blitz Computer Game");
		setLayout(new BorderLayout());
		setSize(800, 800);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//setLayout(new GridBagLayout());
		
		PlayArea pa = new PlayArea();
		
		JPanel j1 = new JPanel();
		add(j1, BorderLayout.WEST);
		ComputerPlayer p1 = new ComputerPlayer(pa, j1);
		//HumanPlayer p1 = new HumanPlayer(pa, j1);
		p1.start();
		
		JPanel j2 = new JPanel();
		add(j2, BorderLayout.NORTH);
		ComputerPlayer p2 = new ComputerPlayer(pa, j2);
		//HumanPlayer p2 = new HumanPlayer(pa, j2);
		p2.start();
		
		add(pa, BorderLayout.CENTER);
		
		JPanel j3 = new JPanel();
		add(j3, BorderLayout.EAST);
		ComputerPlayer p3 = new ComputerPlayer(pa, j3);
		//HumanPlayer p3 = new HumanPlayer(pa, j3);
		p3.start();
		
		JPanel j4 = new JPanel();
		add(j4, BorderLayout.SOUTH);
		ComputerPlayer p4 = new ComputerPlayer(pa, j4);
		//HumanPlayer p4 = new HumanPlayer(pa, j4);
		p4.start();

		//HumanPlayer p4 = new HumanPlayer(pa);
		
		//add(p1, BorderLayout.WEST);
		//add(p2, BorderLayout.NORTH);
		//add(p3, BorderLayout.EAST);
		//add(p4, BorderLayout.SOUTH);
	}


}
